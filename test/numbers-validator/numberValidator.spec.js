import {expect} from 'chai';
import NumbersValidator from '../../app/numbers_validator.js';

let validator;

beforeEach(() => {
  validator = new NumbersValidator();
});

afterEach(() => {
  validator = null;
});

describe('Positive cases', function() {
  it('Happy path- isNumberEven', function() {
    expect(validator.isNumberEven(2)).to.be.equal(true);
  });
  it('Should return an array of even numbers only', function() {
    expect(validator.getEvenNumbersFromArray([1, 2, 3, 4])).to.include.members([
      2, 4,
    ]);
  });
  it('Should return true when All the elementes are numbers', function() {
    expect(validator.isAllNumbers([1, 2, 3, 4])).to.be.equal(true);
  });
  it('Should return true when the passed value is integer', function() {
    expect(validator.isInteger(5)).to.be.equal(true);
  });
});

describe('Negative cases', function() {
  it('Should return false when an Odd Number is passed', function() {
    expect(validator.isNumberEven(3)).to.be.equal(false);
  });
  it('Should return an error if isNumberEven(n) does not receive a number but a string', function() {
    const isNumberEvenError = () => validator.isNumberEven('hello');
    expect(isNumberEvenError).to.throw(
        Error,
        `[hello] is not of type "Number" it is of type "string"`,
    );
  });
  it('Should return an error if It is not array of numbers', function() {
    const getEvenNumbersFromArrayError = () =>
      validator.getEvenNumbersFromArray(['1', '2', '3', '4']);
    expect(getEvenNumbersFromArrayError).to.throw(
        `[1,2,3,4] is not an array of "Numbers"`,
    );
  });
  it('Should return an error if isAllNumber([]) does not receive an Array but a number', function() {
    const isAllNumberError = () => validator.isAllNumbers(2);
    expect(isAllNumberError).to.throw(`[2] is not an array`);
  });
  it('Should return an error if IsInteger does not receive a number but a string', function() {
    const isIntegerError = () => validator.isInteger('hi');
    expect(isIntegerError).to.throw(`[hi] is not a number`);
  });
});
